# Function for identifying if call is first
cntxt_A <- function(call_str, call){
  call_str[1] == call
}

# Function for identifying if call is following
cntxt_B <- function(call_str, call){
  if(length(call_str) > 1){
    call %in% call_str[2:length(call_str)]
  }else{
    FALSE
  }
}

# Prepare dataset
data <- binomngram_preproc(
  ngram_lst = bosshardEtAl2022_largeRecombination,
  cntxt_A = cntxt_A,
  cntxt_B = cntxt_B,
  call_A = "Peep",
  call_B = "Howl")
print(data)

# Run Collocation Analysis
res <- binomngram(data = data)
print(res)
