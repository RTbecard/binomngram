bosshardEtAl2022_largeExclusive <- read_ngrams(
  "~/Dropbox/Projects/202212_Meike_CallAnalysis/bosshard_data_largeExclusive.txt",
  sep = " ")
bosshardEtAl2022_largeRecombination <- read_ngrams(
  "~/Dropbox/Projects/202212_Meike_CallAnalysis/bosshard_data_largeRecombination.txt",
  sep = " ")
bosshardEtAl2022_smallExclusive <- read_ngrams(
  "~/Dropbox/Projects/202212_Meike_CallAnalysis/bosshard_data_smallExclusive.txt",
  sep = " ")
bosshardEtAl2022_smallRecombination <- read_ngrams(
  "~/Dropbox/Projects/202212_Meike_CallAnalysis/bosshard_data_smallRecombination.txt",
  sep = " ")

save(
  bosshardEtAl2022_largeExclusive,
  bosshardEtAl2022_largeRecombination,
  bosshardEtAl2022_smallExclusive,
  bosshardEtAl2022_smallRecombination,
  file = "./data/bosshardEtAl2022.rda")




binom.test(x = 2, n = 10, p = 0.6, alternative = "two.sided")
pbinom(q = 2, size = 10, prob = 0.6, lower.tail = T) + pbinom(q = 4, size = 10, prob = 1-0.6, lower.tail = T)

binom.test(x = 2, n = 10, p = 0.5, alternative = "less")
pbinom(q = 2, size = 10, prob = 0.5, lower.tail = T)


binom.test(x = 5, n = 10, p = 0.56, alternative = "two.sided")

qbinom(0.3438, 10, 0.5, lower.tail = T)

