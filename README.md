# Binomial N-gram Analysis for Collocation Strength

Install and view the package documentation.

```r
require(devtools)

# Install package
install_git(
  "https://gitlab.com/RTbecard/binomngram.git", 
  build_vignettes = T, 
  build_manual = T, 
  force = T)

# View vingette
browseVignettes(package = "binomngram", all = TRUE)
```

